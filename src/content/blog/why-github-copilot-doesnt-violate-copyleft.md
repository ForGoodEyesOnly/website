---
title: Why GitHub Copilot doesn't violate free software licences
layout: redirect-min
url: /blog/2022/07/why-github-copilot-doesnt-violate-free-licences/
redirect:
  target: "https://blog.forgoodeyesonly.eu/2022/07/why-github-copilot-doesnt-violate-copyleft/"
---