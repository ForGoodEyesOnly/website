---
title: Häufig gestellte Fragen
tabTitle: FAQ | For-Good-Eyes-Only-Lizenz
description: Ist die Lizenz open-source? Warum erzwingt sie kein Copyleft? Antworten auf Fragen zur freien ethischen For-Good-Eyes-Only-Lizenz.
image: img/logo/logo-1920.png
imageDescription: Das Logo der Lizenz mit einem orangen-roten Olivenkranz um eine wehende blaue Flagge mit einer weißen Taubenfeder, wobei der orangene Buchstabe J als Mast dient.
layout: article
meta: true
faq: true
toc: true
---

<p>

## Wie lautet der offizielle Name der Lizenz?

Der offizielle Name der Lizenz lautet: <br>`For Good Eyes Only Licence v0.2` <br>oder kurz: `FogEye v0.2`

## Kann ich die Lizenz für meine eigenen Werke verwenden?

Ja, bitte! Verwende gern den Namen, Text, die Zusammenfassung, Icons, Logos und Banner der Lizenz, um deine Werke zu 
lizenzieren. 
Kopiere dazu einfach die Lizenzdatei in dein Repository und gib in deiner README an, dass du die Lizenz 
verwendest. 
Du kannst den Lizenztext auch abändern, musst dann aber auch den Namen, die Icons, Logos und Banner 
entsprechend anpassen.

## Kann ich meine eigene Lizenz auf Basis dieser verfassen?

Natürlich! Achte aber darauf, den Namen, Icons, Logos und Banner zu ändern. Weitere Details findest du im
{{< link "v0.2" >}}Lizenztext{{< /link >}}.

## Welchen Landes Recht gilt für die Lizenz?

Siehe dazu den Abschnitt „Scope“ der Lizenz:

> To this Licence the law of the Licensee’s jurisdiction, from which the Permission was granted, shall apply.

## Ist die Lizenz eine Freie-Software-Lizenz?

Die For-Good-Eyes-Only-Lizenz ist eine ethische Lizenz. Das bedeutet, dass die Softwarefreiheiten der FSF denen 
gewährt werden, die sich an Prinzipien von Ethik, Gerechtigkeit und Fairness halten.

## Ist die Lizenz quelloffen?

Der Autor der Lizenz erkennt die „Open-Source-Definition“ der sogenannten „Open-Source-Initiative“ nicht als
allgemeingültige Bedeutung des Begriffs „quelloffen“ an, da dieser im alltäglichen Sprachgebrauch meist nur bedeutet,
dass der Quellcode eines Programms öffentlich zugänglich ist, was nicht zwangsläufig heißt, dass er frei verwendet
werden darf. 
Da eine Lizenz kein Programm ist, kann sie nicht quelloffen sein.

## Warum erzwingt die Lizenz kein Copyleft?

Für den Autor der Lizenz bedeutet Softwarefreiheit nicht nur, Programme frei verwenden zu können, sondern auch, andere
Entwickler nicht in ihrer Freiheit einzuschränken, ihre eigenen Werke unter eigenen Bedingungen zur Verfügung zu
stellen.
Außerdem könnte das Erzwingen von Copyleft in der Praxis zu Kompatibilitätsproblemen mit Software von
Drittanbietern führen.

## Kann ich meine Werke unter mehrere Lizenzen stellen?

Da du der Urheber bist, kannst du frei entscheiden, unter welchen Bedingungen du deine Werke anderen zur Verfügung
stellst. 
Du kannst also mehrere Lizenzen für deine Werke wählen.

## Kann ich meine Werke unter For Good Eyes Only v0.2 *„or later“* lizenzieren?

Nein, lieber nicht. Zukünftige Versionen der Lizenz könnten unvorhergesehene Änderungen enthalten, weshalb du im 
Falle des Erscheinens einer neuen Lizenzversion diese händisch übernehmen solltest – und das auch nur, nachdem du
sie vollständig gelesen und verstanden hast.

## War die Lizenz schon einmal Gegenstand eines Gerichtsverfahrens?

Nach dem Wissen des Autors der Lizenz war die Lizenz noch nie Gegenstand eines Gerichtsverfahrens.
Es wäre jedoch ein großer Fehler anzunehmen, dass ein Gerichtsurteil in irgendeiner Weise Rechtssicherheit
garantieren würde.
Zum einen gilt in den meisten Ländern die richterliche Unabhängigkeit, weshalb Richter beispielsweise in Deutschland 
nicht gezwungen sind, der Gesetzesauslegung anderer Gerichte zu folgen.
Zum anderen kann es zwar durchaus vorkommen, dass sich zwei Fälle einander ähneln, es wird jedoch niemals zwei
völlig identische geben, weshalb kein Gerichtsurteil jemals universelle Gültigkeit erlangen kann.
Außerdem wäre ein Urteil in jedem Fall nur auf das jeweilige Land anwendbar und nicht auf andere Staaten, zumal
sich die Gesetzeslage von Land zu Land unterscheidet, wenngleich marginal.

## Welche anderen Lizenzen haben diese inspiriert?

Diese Lizenz wurde inspiriert von der [Hippocratic Licence](https://firstdonoharm.dev), 
der [Do No Harm Licence](https://github.com/raisely/NoHarm) und den 
[Atmosphere Licences](https://www.open-austin.org/atmosphere-license/about/index.html).

## Warum ist die Lizenz in Englisch verfasst?

Englisch ist weltweit die lingua franca schlechthin, vor allem weil sie so einfach zu erlernen ist – natürlich auch für
den Autor der Lizenz.
Gerade in der Softwareentwicklung ist ein veritables Verständnis der englischen Sprache unabdingbar, da alle
ernstzunehmenden Programmiersprachen und Frameworks auf Basis englischer Begriffe entwickelt werden.
Zudem sind technische Dokumentationen zumeist in englischer Sprache verfasst, weswegen sie die naheliegendste Wahl für
Softwarelizenzen ist.
