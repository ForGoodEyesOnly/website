---
title: Frequently Asked Questions
tabTitle: Frequently Asked Questions | For Good Eyes Only Licence
description: Is it open source? Why doesn’t it enforce copyleft? Answers about the free ethical For Good Eyes Only Licence.
image: img/logo/logo-1920.png
imageDescription: The licence’s logo with an orange-red olive branch wreath around a waving blue flag with a white dove wing, with the orange letter J serving as the mast.
layout: article
meta: true
faq: true
toc: true
---

<p>

## What’s the official name?

The official name of this licence is: <br>`For Good Eyes Only Licence v0.2`

You can also abbreviate it to: `FogEye v0.2`

## Can I use the licence myself?

Yes, please! Feel free to use the licence’s name, text, summary, icons, logos and banners for licensing your works. Simply copy the licence file to your repository and state in your README that you use the licence. You may also change the licence’s text, but then you have to change its name, icons, logos and banners too.

## Can I develop my own licence based on this one?

Yes, please! Just make sure to change its name, icons, logos and banners. For more details, have a look at the
licence notice at the beginning of the {{< link "v0.2" >}}licence text{{< /link >}}.

## Which country’s law applies to the licence?

For reference, see the licence’s “Scope” section:

> To this Licence the law of the Licensee’s jurisdiction, from which the Permission was granted, shall apply.

## Is it a Free Software licence?

It is an Ethical Licence, which means that the software freedoms of the FSF are granted to those who adhere to principles of ethics, justice and fairness.

## Is the licence open source?

The licence’s author does not acknowledge the “Open Source Definition” of the so-called “Open Source Initiative” as the universally valid meaning of the term “open source”, since in everyday language “open source” mostly only means that a program’s source code is publicly accessible, which does not necessarily imply that it may be used freely. Since a licence is not a piece of software, it cannot be open source.

## Why doesn’t the licence enforce a copyleft policy?

To the licence’s author software freedom isn’t only about being able to freely use programs, but also about not restricting other developers in their freedom to make their own creations available under custom conditions. Also, enforcing a copyleft policy might cause compatibility issues with third-party software in practice.

## Can I dual-license my works?

Since you are the copyright holder, you can freely choose the conditions under which you make your work available to others. So, yes, you can choose multiple licences for your works.

## Can I license my works under For Good Eyes Only v0.2 “or later”?

No, don’t do that. Future versions of this licence may contain unforeseen changes, so you ought to update the licence manually – and only after you have read and understood it fully.

## Is the licence court-proven?

As far as I know, the licence has never been subject to any litigation. However, it would be a gross mistake to assume that a court ruling would in any way guarantee legal certainty. First of all, judges in most jurisdictions are independent and, for example in Germany, are not forced to follow the interpretation of the law by other courts. Moreover, while two cases may be similar, they will never be identical, which is why a verdict should in no way be considered universally valid. Also, a ruling would in any case only be applicable to the jurisdiction in question and not to other states.

## Which other licences inspired this one?

This licence was inspired by the [Hippocratic Licence](https://firstdonoharm.dev), the [Do No Harm Licence](https://github.com/raisely/NoHarm) and the [Atmosphere Licences](https://www.open-austin.org/atmosphere-license/about/index.html).

## Why do you spell “license” incorrectly?

The term in question is spelled “licence” in original English, cf. [Cambridge Dictionary](https://dictionary.cambridge.org/dictionary/english/licence).