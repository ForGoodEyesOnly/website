---
title: For Good Eyes Only
layout: home
---

{{< headline "1" "top" >}}Another Free Ethical Licence{{< /headline >}}

With this Free Ethical Licence for [Ethical Source Software](https://ethicalsource.dev/principles), developers can make their source code available to others free of charge and for any purpose, but without losing sight of their responsibility for the code. Namely, the licence restricts permission to only those licensees who act in conformity with certain ethical principles, in order to promote a sustainable and dignified world.

For more information, read the {{< link "v0.2" >}}full licence text{{< /link >}} and the corresponding {{< link "FAQ" >}}FAQ{{< /link >}}. Also, feel free to download the licence in any of these formats:

{{< licenceFormats "v0.2" >}}

{{< defence >}}

## Preamble

Conscious of his/her responsibility before God and humankind, impelled by the will to serve the peace of the world, the Licensor, by virtue of his/her authorship rights, has placed his/her work under this For Good Eyes Only Licence. The human dignity shall be inviolable. Respecting it is the obligation of all power of the Licensee and the Licensor. The Licensee and the Licensor thus profess inviolable and inalienable human rights as the foundation of every human community, of peace and of justice in the world.

{{< licenceSummary >}}