---
title: For Good Eyes Only
layout: home
---

{{< headline "1" "top" >}}Freie ethische Lizenz{{< /headline >}}

Mit dieser freien ethischen Lizenz für [Ethical-Source-Software](https://ethicalsource.dev/principles) können 
Entwickler ihren Quellcode anderen kostenlos und für jeden Zweck zur Verfügung stellen, ohne dabei ihre Verantwortung 
für den Code aus den Augen zu verlieren. 
Die Lizenz beschränkt nämlich die Verwendungserlaubnis auf nur diejenigen Lizenznehmer, die bestimmte ethische
Grundsätze respektieren, um eine nachhaltige und würdige Welt zu fördern.

Lies den [vollständigen Lizenztext](/v0.2) und die dazugehörigen 
{{< link "FAQ" >}}FAQ{{< /link >}} für mehr Informationen. 
Außerdem kannst du die Lizenz in folgenden Formaten herunterladen:

{{< licenceFormats "v0.2" >}}

{{< defence >}}

## Präambel

Im Bewusstsein seiner Verantwortung vor Gott und den Menschen, von dem Willen beseelt, dem Frieden der Welt zu dienen,
hat der Lizenzgeber kraft seines Urheberrechts sein Werk unter diese For-Good-Eyes-Only-Lizenz gestellt.
Die Würde des Menschen ist unantastbar. Sie zu achten und zu schützen ist Verpflichtung aller Gewalt des Lizenznehmers
und des Lizenzgebers.
Der Lizenznehmer und der Lizenzgeber bekennen sich darum zu unverletzlichen und unveräußerlichen Menschenrechten als
Grundlage jeder menschlichen Gemeinschaft, des Friedens und der Gerechtigkeit in der Welt.

{{< licenceSummary >}}