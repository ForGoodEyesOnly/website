---
title: "{{ replace .Name "-" " " | title }}"
draft: false
date: '{{ .Date }}'
year: '{{ .Date | dateFormat "2006" }}'
month: '{{ .Date | dateFormat "2006-01" }}'
---
