#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

publishDir="$1"
source "deploy_config.txt"

sftp "$ssh_server":/tmp/ <<EOF
put -r "$publishDir"
exit
EOF

ssh -t "$ssh_server" "sudo trash-put $deploy_dir"
ssh -t "$ssh_server" "sudo mv /tmp/$(basename "$publishDir") $deploy_dir"
ssh -t "$ssh_server" "sudo chown -R $deploy_user $deploy_dir"
