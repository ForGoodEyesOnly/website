# For Good Eyes Only

For more information about the For Good Eyes Only Licence, please visit the [For Good Eyes Only](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only) repository.

For information about the licence under which you may use this repo, pleae see [For Good Eyes Only/website](https://codeberg.org/ForGoodEyesOnly/website).