#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

cat config/_default/base.yaml > config/_default/hugo.yaml
cat config/onion/base.yaml > config/onion/hugo.yaml
tee --append config/_default/hugo.yaml config/onion/hugo.yaml < config/other.yaml > /dev/null 2>&1
