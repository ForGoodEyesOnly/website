#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

publishDir="$1"

# get SHA256 hash of current date
now=$(date +"%Y-%m-%d--%H:%M:%s:%N")
tempFolder="/tmp/$(echo -n "$now" | sha256sum)"

# copy .git folder from publish dir to temp folder
echo "Backing up Git repo of publishDir..."
mkdir "$tempFolder"
cp -r "$publishDir"/.git/ "$tempFolder"

# delete the publish dir
echo "Emptying publishDir..."
trash-put "$publishDir"

# recreate publish dir and restore .git folder
echo "Restoring Git repo..."
mkdir "$publishDir"
cp -r "$tempFolder"/.git "$publishDir"

# remove temp folder
trash-put "${tempFolder[@]}"
