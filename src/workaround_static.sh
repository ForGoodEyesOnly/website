#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Avoid Hugo error: "blog/index.xml: is a directory"
trash-put "../../apex-pages/blog/index.xml"
trash-put "../../apex-pages-onion/blog/index.xml"
mv "../../apex-pages/blog/indexdotxml" "../../apex-pages/blog/index.xml"
mv "../../apex-pages-onion/blog/indexdotxml" "../../apex-pages-onion/blog/index.xml"
