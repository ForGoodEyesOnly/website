# For Good Eyes Only Licence

For more information about the licence, please see the [For Good Eyes Only](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only) repository.

## Licence

Where not stated otherwise, any content created by Pixelcode in this repository may be used according to the [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/), provided that, in case you change the licence text, you make its name, summary, icons, logos and banners distinguishable from the original licence.

### HugoCommit.sh

The For Good Eyes Only Licence's website uses `HugoCommit.sh` created by Pixelcode to make the Hugo building process a bit more easy. In particular, the script files `HugoCommit.sh` and `commit.sh` are [MIT-licensed](https://codeberg.org/pixelcode/HugoCommit.sh#licence) (slightly modified).

### Third-party photos

Background photo of “Mass Surveillance” promo banner: &copy; [Pexels](https://pixabay.com/users/pexels-2286921/) on [Pixabay](https://pixabay.com/photos/building-cctv-door-female-ladies-1839464), [Pixabay Licence](https://pixabay.com/service/license/)

Background photo of “Fake News” promo banner: &copy; [Gerd Altmann](https://pixabay.com/users/geralt-9301) on [Pixabay](https://pixabay.com/photos/trump-president-usa-america-flag-2546104/), [Pixabay Licence](https://pixabay.com/service/license/)

Background photo of “Climate Change” promo banner: &copy; U.S. Department of Agriculture on [Flickr](https://www.flickr.com/photos/usdagov/9665091714/in/photolist-KtMjQH-Lg9bSL-KZ4K3G-oZPLW9-Lg8yJU-JBqPGf-ffmhft-nRY7mK-HJj89N-LqsK5K-HJiZgU-LqsJYn-Lg9bVw-LnwBru-Jvs7RL-ffmkhi-Lg9bPE-KnRFBA-LqsJZp-ffmk98-nzMpVB-ffmkeZ-nzLsmT-ffmh2k-nzMpQX-Lg8LDA-fCfjzp-ffAxYE-oZPRVX-JBryby-JBrEP3-JBrpZU-xAy2xo-phgZyu-oZP6Dy-JBqPFd-JEugia-fJ583j-KtvXoA-GjLhZu-ggvJFP-yUyxZh-ynjXPJ-HJf2AF-G81heK-nzLB91-G81hgP-JBqDKw-U87a83-ffmhci/), [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)